﻿using System;

namespace EjerciciosdelaClase
{

    class ProgramaCliente
    {
        private string nombre;
        private int monto;
        public ProgramaCliente(string n1)
        {
            nombre = n1;
            monto = 0;
        }
        public void Depositar(int m2)
        {
            monto = monto + m2;
        }
        public void Extraer(int m2)
        {
            monto = monto - m2;
        }
        public int RetornarMonto()
        {
            return monto;
        }
        public void Imprimir()
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine(nombre + " ha hecho un deposito de : " + monto);
        }
    }
    class Banco
    {
        private ProgramaCliente cliente1, cliente2, cliente3;
        public Banco()
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            cliente1 = new ProgramaCliente("Emmanuel");
            cliente2 = new ProgramaCliente("Isabel");
            cliente3 = new ProgramaCliente("Miguel");
        }
        public void Operar()
        {
            cliente1.Depositar(25000);
            cliente2.Depositar(44000);
            cliente3.Depositar(18500);
            cliente3.Extraer(6700);
        }
        public void DepositosTotales()
        {
            
            int ac = cliente1.RetornarMonto() +
                    cliente2.RetornarMonto() +
                    cliente3.RetornarMonto();
            Console.WriteLine("El Total de Dinero en el banco es:" + ac);
            cliente1.Imprimir();
            cliente2.Imprimir();
            cliente3.Imprimir();
        }
        static void Main(string[] args)
        {
            
            Console.WriteLine("-----------------------------" +
                             "---Welcome to the ITLA Bank---" +
                             "------------------------------");
            Console.WriteLine();
            Banco bancox = new Banco();
            bancox.Operar();
            bancox.DepositosTotales();
            Console.ReadKey();
        }
    }
}